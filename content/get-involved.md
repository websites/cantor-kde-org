---
title: Get Involved
name: Cantor
menu:
  main:
    weight: 5
layout: get-involved
userbase: Cantor
getintouch: |
  Most development-related discussions take place on the [Cantor mailing list](http://mail.kde.org/mailman/listinfo/kde-edu)

  Just join in, say hi and tell us what you would like to help us with!
---
