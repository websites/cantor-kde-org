---
title: User support
menu:
  main:
    weight: 4
layout: users
forum: https://discuss.kde.org
handbook: http://docs.kde.org/stable/en/kdeedu/cantor/index.html
name: Cantor
---
