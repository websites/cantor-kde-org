---
layout: post
title: Proposal discussion - Python 3 as the only backend officially supported
date: 2018-01-08
---

The Cantor maintainer Filipe Saraiva [wrote a post](http://blog.filipesaraiva.info/?p=1897) to discuss the future of Cantor. The main idea in the text is focus effort only in Python 3 backend in order to provide a better experience to the users and allow the work in new features for Cantor itself.

The other backends would be moved to a third-party repository and they will not be officially maintained. They could be available as extensions in KDE Store if someone would like to maintain some of them.

This is just a proposal and it is open to discussions. Please, join the conversation in Filipe's blogpost or in [KDE-Edu mailing list](https://mail.kde.org/mailman/listinfo/kde-edu).
