---
layout: post
title: Cantor in KDE Applications 19.04
date: 2019-04-18
---

Cantor new version was released with [KDE Applications 19.04](https://kde.org/announcements/announce-applications-19.04.0.php) bundle. The main changes are:

* Possibility to hide and show results of command entry via context menu;
* Add a way to specify the path to the local documentation for Maxima, Octave, Python and R backends;
* Huge improvements in variable management.

Read the complete changelog for this version of Cantor in [KDE Applications 19.04 Full Log Page](https://www.kde.org/announcements/fulllog_applications.php?version=19.04.0).
