---
layout: post
title: Cantor in KDE Applications 17.12
date: 2017-12-14
---

Cantor new version was released with [KDE Applications 17.12](https://www.kde.org/announcements/announce-applications-17.12.0.php) bundle. This version is more focused in stability and bug fixes.

Read the complete changelog for this version of Cantor in [KDE Applications 17.12 Full Log Page](https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0).
