---
layout: post
title: Cantor in kdereview
date: 2009-10-01
---

Cantor is now in kdereview in order to be peer-reviewed before joining KDE-Edu
for the KDE 4.4.0 release! you are encouraged to try it and report any problems
to the author.
