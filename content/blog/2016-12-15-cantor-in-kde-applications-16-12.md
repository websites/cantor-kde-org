---
layout: post
title: Cantor in KDE Applications 16.12
date: 2016-12-15
---

Cantor new version was released with [KDE Applications 16.12](https://www.kde.org/announcements/announce-applications-16.12.0.php) bundle. This version has an interesting new backend and several bugfixes. The main changes are:

* New backend: Julia, developed by Ivan Lakhtanov during his GSoC 2016. More infos in [Ivan's blog](https://juliacantor.blogspot.com.br/2016/08/cantor-gets-support-of-julia-language.html);
* Fix syntax highlight of strings and comments in Python backend;
* Introducing a new feature: recommended programming language version in backend description;
* Fix the crash after close a session of Python, Scilab, or Julia backends;
* More robust version check of Sage.

A few more bugs were solved too. Read the complete changelog for this version of Cantor in [KDE Applications 16.12 Full Log Page](https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0).

Happy Holidays!
