---
layout: post
title: Cantor in KDE Applications 16.04
date: 2016-04-20
---

The new version of Cantor was released with [KDE Applications 16.04](https://www.kde.org/announcements/announce-applications-16.04.0.php). The main changes are:

* Now Cantor is using the version number of KDE Applications version;
* Support to Sage > 6.5 version;
* Fix Octave tab completion;
* The authors will update Cantor website to inform the news of the project;
* Filipe Saraiva is the new maintainer.

The complete changelog of Cantor can be read in [KDE Applications 16.04 Full Log Page](https://www.kde.org/announcements/fulllog_applications.php?version=16.04.0#cantor).

We would like to improve the Cantor development, calling developers to coordinate the bugs hunt and the development of new features. Wait for more news soon!
