---
layout: post
title: Cantor 19.08
date: 2019-08-15
---

*(Original post written by Alexander Semke in [Labplot's blog](https://labplot.kde.org/2019/08/15/cantor-19-08/))*

Since the last year the development in Cantor is keeping quite a good momentum. After many new features and stabilization work done in the 18.12 release, see this [blog post](https://labplot.kde.org/2018/12/21/cantor-18-12-kde-way-of-doing-mathematics/) for an overview, we continued to work on improving the application in 19.04. Today the release of [KDE Applications 19.08](https://kde.org/announcements/announce-applications-19.08.0.php), and with this of Cantor 19.08, was announced. Also in this release we concentrated mostly on improving the usability of Cantor and stabilizing the application. See the [ChangeLog](https://cgit.kde.org/cantor.git/tree/ChangeLog) file for the full list of changes.

For new features targeting at the usability we want to mention the **improved handling of the “backends”**. As you know, Cantor serves as the front end to different open-source computer algebra systems and programming languages and requires these backends for the actual computation. The communication with the backends is handled via different plugins that are installed and loaded on demand. In the past, in case a plugin for a specific backend failed to initialize (e.g. because of the backend executable not found, etc.), we didn’t show it in the “Choose a Backend” dialog and the user was completely lost. Now we still don’t allow to create a worksheet for this backend, but we show the entry in the dialog together with a message about why the plugin is disabled. Like as in the example below to check the executable path in the settings:


<img src="/assets/img/backend_requirements_maxima.png" class="img-fluid" />

Similar for cases where the plugin, as compiled and provided by the Linux distribution, doesn’t match to the version of the backend that the user installed manually on the system and asked Cantor to use. Here we clearly inform the user about the version mismatch and also advise what to do:

<img src="/assets/img/backend_requirements_julia.png" class="img-fluid" />

Having mentioned custom installations of backends and Julia above, in 19.08 we allow to set the **custom path to the Julia interpreter** similarly to how it is already possible for some other backends.

The **handling of Markdown and LaTeX entries** became more comfortable. We allow now to quickly switch from the rendered result to the original code via the mouse double click. Switching and back, as usual, via the evaluation of the entry. Furthermore, the results of such **rendered Markdown and LaTeX entries are saved now as part of the project**. This allows to consume projects with such Markdown and LaTeX entries also on systems having no support for Markdown and LaTeX rendering process. This also decreases the loading times of projects since the ready-to-use results can be directly used.

In 19.08 we added the **“Recent Files” menu** allowing for a quick access to the recently opened projects:

<img src="/assets/img/recent_files_menu.png" class="img-fluid" />

Among important bug fixes we want to mention fixes that **improved the communication with the external processes** “hosting” embedded interpreters like Python and Julia. Cantor reacts now much better on errors and crashes produced in those external processes. For Python the interruption of running commands was improved.

While working on 19.08 to make the application more usable and stable we also worked on some bigger new features in parallel. This development is being done as part of Google Summer of Code project and the goal of this project is to add the **support of Jupyter notebooks** in Cantor. The idea behind this project and its progress are covered in a series of blogs ([here](https://sirgienkogsoc2019.blogspot.com/2019/06/support-for-jupyter-notebooks-has.html), [here](https://sirgienkogsoc2019.blogspot.com/2019/07/improved-rendering-of-mathematical.html) and [here](https://sirgienkogsoc2019.blogspot.com/2019/07/markdown-and-support-of-embedded.html). The code is in a quite good shape and was merged to master already. This is how such a Jupyter notebook looks like in Cantor:

<img src="/assets/img/AsvB7QU-768x482.png" class="img-fluid mx-auto d-block" />

We plan to release this in the upcoming release of KDE Applications 19.12. Stay tuned!
