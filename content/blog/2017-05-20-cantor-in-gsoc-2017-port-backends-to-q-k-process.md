---
layout: post
title: Cantor in GSoC 2017 - Port backends to Q/KProcess
date: 2017-05-20
---

Cantor was accepted to Google Summer of Code 2017!

This year we have an Indian student working with us, Rishabh Gupta. His project is port all Cantor backends to use [QProcess](http://doc.qt.io/qt-5/qprocess.html) or [KProcess](https://api.kde.org/frameworks/kcoreaddons/html/classKProcess.html) (we are [investigating](https://phabricator.kde.org/T6111) which is more suitable for Cantor).

Currently Cantor backends are implemented using a set of different technologies like KProcess (Scilab, Octave, and Sage backends), DBus (Julia, Python 3 and R), and own languages APIs (Python 2, Lua, and KAlgebra). Because this it is hard to support correctly all of these backends, and it is hard to port Cantor to different operating systems.

If we define a standard technology to implement backends, we belive it will be more easy to maintain Cantor and port the software to others OS platforms. QProcess and KProcess are interesting alternatives to this because they have good support in different OS platforms.

You can follow the work of Rishabh in [his blog](https://rish9511.wordpress.com/) and track the tasks related to this project in [this task](https://phabricator.kde.org/T6110) at KDE Phabricator.

Have a good work Rishabh and welcome to Cantor hackers team!
