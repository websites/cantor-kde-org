---
layout: post
title: Cantor in GSoC 2016 - Backend for Julia
date: 2016-04-22
---

Cantor was accepted to Google Summer of Code 2016!
    
This year we have a Russian student working with us, Ivan Lakhtanov. His project is develop a backend for [Julia](http://julialang.org/). If you want to follow the work of Ivan, please keep your eyes in [his blog](http://juliacantor.blogspot.ru/).

Have a good work Ivan and welcome to Cantor and KDE!
