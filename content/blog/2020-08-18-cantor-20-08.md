---
layout: post
title: Cantor 20.08
date: 2020-08-18
---

Cantor, the frontend to scientific interactive computing developed by KDE community, got a new release this month. Our developers are adding some usability improvements and some [initial results from GSoC projects](https://cantor.kde.org/2020/06/13/cantor-in-gsoc-2020.html) are now available with the 20.08 release.

For example, now you can collapse, uncollapse, and remove all results from the worksheet; exclude entries from the worksheet commands processing; add actions for selected texts; zoom widgets; get tooltips for almost all settings options; use the new horizontal rule entry; and more.

<p class="text-center">
    <img src="/assets/img/tooltips-settings.gif" alt="Tooltips in settings dialog" />
</p>

Python, Octave and Julia backends now support more image formats and popular plotting packages, such as GR, plots, PyPlot and Gadfly for Julia; and matplotlib, pylab, plot.ly, GR and bokep for Python.

<p class="text-center">
    <img src="/assets/img/python-plotting-backends.png" alt="Python plotting packages" />
</p>

We added a new panel for the file browser, so you can navigate in folders and supported files from inside Cantor.

Finally, images are now saved in Cantor worksheets files and the worksheets can be shared with the images embedded as expected!

We hope you enjoy this release. Please, let us know if you have find a bug or have any questions or feature requests by contacting us through our [user support channels](https://cantor.kde.org/users/).
