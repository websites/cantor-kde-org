---
layout: post
title: Cantor in GSoC 2020
date: 2020-06-13
---

KDE is once again [taking part in Google Summer of Code program](https://summerofcode.withgoogle.com/organizations/4610988642402304/) and this time Cantor has 2 internships working to improve the software and bringing new features. Both projects are supervised by Alexander Semke and Stefan Gerlach.

[Nikita Sirgienko](https://summerofcode.withgoogle.com/projects/#5069417982984192) is polishing usability and developing several small features present in other mathematical [REPL applications](https://en.wikipedia.org/wiki/Read-eval-print_loop) to improve the user experience in Cantor. In his words, "_the idea of this project is not to implement one single and big "killer feature" but to address several smaller and bigger open and outstanding topics in Cantor_".

[Shubham](https://summerofcode.withgoogle.com/projects/#4689937489723392) is bringing the documentations of the programming languages supported by Cantor to the application itself. Currently, Cantor just provides a link to the documentations websites, and obviously it can be improved. When this project be successed, it will be possible to provide documentation search and context sensitive help facilities.

Follow [Nikita project's blog](https://cantorgsoc2020.blogspot.com/) and [Shubham project's blog](https://coderunner20.blogspot.com/) for news about their progress.

Happy hacking!
