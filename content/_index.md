---
title: Cantor
subtitle: KDE's Frontend to Mathematical Applications
noSubpage: true
---

**Cantor** is an application that lets you use your favorite mathematical programming language from within a nice worksheet interface. It offers assistant dialogs for common tasks and allows you to share your worksheets with others.

![](/assets/img/screenshot.png)

## Features

{{< feature-container img="/assets/img/backends.gif" >}}

### Support for several backends

Cantor supports several backends - mathematical programming languages used to run the commands. Currently Cantor supports 10 backends - <a href='http://julialang.org/'>Julia</a>, <a href='http://edu.kde.org/kalgebra/'>KAlgebra</a>, <a href='http://lua.org/'> Lua</a>, <a href='http://maxima.sourceforge.net/'>Maxima</a>, <a href='https://gnu.org/software/octave/'>Octave</a>, <a href='http://python.org/'>Python</a>, <a href='http://qalculate.sourceforge.net/'>Qalculate</a>, <a href='http://r-project.org/'>R</a>, <a href='http://sagemath.org/'>Sage</a>, and <a href='http://scilab.org/'>Scilab</a> - and more backends can be added just implementing a new Cantor plugin!

{{< /feature-container >}}

{{< feature-container img="/assets/img/processing.gif" reverse="true" >}}

### Rich command processing

Cantor allows you to run commands from your favorite mathematical programming language assisted by syntax highlighting and tab-completion for functions, reserved words and variables. See the output of your work directly inside the worksheet or export to PDF, LaTeX, plain-text, and Jupyter notebooks!

{{< /feature-container >}}

{{< feature-container img="/assets/img/variables.gif" >}}

### Quick variables management

Manage all your variables from the built-in side panel. See the values, size, and type of variables utilized in the environment at a glance. In addition, it is possible to change the variables values from the side panel.

{{< /feature-container >}}

{{< feature-container img="/assets/img/plotting.gif" reverse="true" >}}

### Plotting graphics in the worksheet

Making nice looking charts and graphics is easy in Cantor thanks to the powerful plotting features. Regardless if you want to visualize a small set of numbers or work with massive amounts data, Cantor has you covered.

{{< /feature-container >}}

{{< feature-container img="/assets/img/rendering.gif" >}}

### Rendering outputs in LaTeX or Markdown

Cantor allows you to write beautiful texts using LaTeX or Markdown. Now your worksheet can be both beautiful and easy to write!

{{< /feature-container >}}

{{< feature-container img="/assets/img/help.gif" reverse="true" >}}

### And more!

* Easy plugin based structure to add different backends
* Integration to retrieve and share worksheets
* Plugin based assistant dialogs for common tasks (like integrating a function or entering a matrix)
* Integration of the environment's help in the user interface
* Script editor for work with large-scale code
* ...

{{< /feature-container >}}