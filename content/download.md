---
layout: download
title: Download
appstream: org.kde.cantor
name: Cantor
menu:
  main:
   weight: 3

sources:
  - name: Release Sources
    src_icon: /reusable-assets/ark.svg
    description: >
        Contor is released regularly as part of KDE Applications. You can 
        find the latest stable release among the 
        <a href="https://download.kde.org/stable/release-service/">KDE Application tarballs</a>.

        If you want to build Calligra from sources, we recommend checking our
        <a href="https://invent.kde.org/education/cantor#how-to-build-and-install-cantor">README</a> which contains instructions for compiling.
  - name: Git
    src_icon: /reusable-assets/git.svg
    description: >
        The git repository can be viewed on
        <a href="https://invent.kde.org/education/cantor">invent.kde.org</a>.

        To clone use <code>git clone https://invent.kde.org/education/cantor</code>.
---
