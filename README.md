# Cantor website

As a (Hu)Go module, it requires both Hugo (currently version 0.134.0 **expanded** is supported) and Go to work.

## Development

Read about the shared theme at [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/).

Since this repo is pretty big, you might want to clone with this command
```
git clone --filter=blob:none git@invent.kde.org:websites/cantor-kde-org.git
```

This will perform a partial clone, reducing the amount of data that needs to be downloaded without usually impacting your workflow. Remove the `--filter=blob:none` part if you want to do a full clone.

Once you have a clone of this repo, you can run this command to try out the site on your local system:

```
hugo server --buildFuture --buildDrafts
```

The `--buildFuture` argument is needed to build content with a date set in the future, and `--buildDrafts` for content with `draft: true`.

## I18n

See [hugoi18n](https://invent.kde.org/websites/hugo-i18n).

## Licensing

We assume new contributions to the content are licensed under CC-BY-4.0 and to the websites code under LGPL-3.0-or-later unless specified otherwise.

